﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AStarPathFinder;
using Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;

#region Cell

public class Cell
{
    public Fleet Occupy;
    public Player Owner { get; set; }
    public int X { get; set; }
    public int Z { get; set; }
    public GameObject plane { get; set; }
    public bool isHomeBase { get; set; }
    public HomeBase homeBase { get; set; }
}

#endregion Cell

#region HomeBase
public class HomeBase
{
    public double supplies { get; set; }
    public double supplyRate { get; set; }
    public Player Owner { get; set; }
    public const int fleetPrice = 10;
    int x { get; set; }
    int z { get; set; }

    public HomeBase(int X, int Z, Player plyr)
    {
        supplyRate = 5;
        x = X;
        z = Z;
        supplies = 0;
        Owner = plyr;
    }

    public void GenerateSupply()
    {
        supplies += supplyRate;
    }

    public void BoostSupply()
    {
        if ((supplies / 2) + supplyRate < supplies)
        {
            supplies -= (supplies / 2) + supplyRate;
            supplyRate *= 0.3;

            #region OLD
            //double multipler = 1.0f, adder = ((supplies - 6) * 0.2f);
            //multipler += adder;

            //if ((supplyRate * multipler) % 1 != 0)
            //{
            //    adder = (supplyRate * multipler) % 1;
            //    adder = (supplyRate * multipler) - adder;
            //}

            //if (supplies >= adder)
            //{
            //    supplies -= adder;
            //    supplyRate += 2;
            //}
            #endregion OLD
        }
    }

    public bool CreateNewFleet(int createDirection, MapManager mapMan)
    {
        bool successful = false;
        switch (createDirection)
        {
            case 0:
                //Do Nothing
                break;

            case 1:
                //North
                successful = CreateFleetOperations(x, z + 1, mapMan);
                break; 

            case 2:
                //North-East
                successful = CreateFleetOperations(x + 1, z + 1, mapMan);
                break;

            case 3:
                //East
                successful = CreateFleetOperations(x + 1, z, mapMan);
                break;

            case 4:
                //South-East
                successful = CreateFleetOperations(x + 1, z - 1, mapMan);
                break;

            case 5:
                //South
                successful = CreateFleetOperations(x, z - 1, mapMan);
                break;

            case 6:
                //South-West
                successful = CreateFleetOperations(x - 1, z - 1, mapMan);
                break;

            case 7:
                //West
                successful = CreateFleetOperations(x - 1, z, mapMan);
                break;

            case 8:
                //North-West
                successful = CreateFleetOperations(x - 1, z + 1, mapMan);
                break;
        }
        return successful;
    }

    public bool CreateFleetOperations(int X, int Z, MapManager mapMan)
    {
        Cell[,] map = mapMan.Main.mapArray;

        if (map[X, Z].plane.GetComponent<Renderer>().sharedMaterial == mapMan.dictVisibleMaterials["Wtr"] && supplies > fleetPrice)
        {
            supplies -= fleetPrice;
            Fleet flt = new Fleet(Owner, false, this);
            flt.Coordinates = new int[] { X, Z };
            flt.ownerNum = Owner.num;
            Owner.fogExceptionAdd(flt.Coordinates);
            Owner.Navy.Add(flt);
            map[X, Z].plane.GetComponent<Renderer>().material = mapMan.Main.GetPlayerShipMaterial(flt.ownerNum, false);
            map[X, Z].Occupy = flt;
            map[X, Z].Owner = Owner;
            mapMan.selectedFleet = flt;
            mapMan.ClickedShipHome(map[X, Z].plane.GetComponent<Collider>());
            mapMan.Main.HandlePlayerFog(Owner.FogOfWar, Owner.fogExceptions, flt.Coordinates);
            return true;
        }
        else
        {
            return false;
        }
    }

    public List<string> GetDirectionalStrings(int[] Coords)
    {
        List<string> arrayDirectionalCoords = new List<string>();

        int X = Coords[0], Z = Coords[1];
        arrayDirectionalCoords.Add(X + "," + (Z + 1));       //North
        arrayDirectionalCoords.Add((X + 1) + "," + (Z + 1)); //NorthEast
        arrayDirectionalCoords.Add((X + 1) + "," + Z);       //East
        arrayDirectionalCoords.Add((X + 1) + "," + (Z - 1)); //SouthEast
        arrayDirectionalCoords.Add(X + "," + (Z - 1));       //South
        arrayDirectionalCoords.Add((X - 1) + "," + (Z - 1)); //SouthWest
        arrayDirectionalCoords.Add((X - 1) + "," + Z);       //West
        arrayDirectionalCoords.Add((X - 1) + "," + (Z + 1)); //NorthWest
        arrayDirectionalCoords.Add(X + "," + Z);             //ShipTile

        return arrayDirectionalCoords;
    }
}
#endregion HomeBase

#region Map
public class Map
{
    public byte[,] pathGrid;
    public Cell[,] mapArray;
    public int size, rowWidth, columnHeight;
    public Dictionary<string, Material> dictVisibleMaterials, dictIllumMaterials, dictFogMaterials;
    GameObject GO;

    public Map(int MapSize, Dictionary<string, Material> _dictVisibleMaterials, Dictionary<string, Material> _dictIllumMaterials, Dictionary<string, Material> _dictFogMaterials, GameObject go)
    {
        pathGrid = new byte[MapSize, MapSize];
        size = MapSize;
        mapArray = new Cell[size, size];
        dictVisibleMaterials = _dictVisibleMaterials;
        dictIllumMaterials = _dictIllumMaterials;
        dictFogMaterials = _dictFogMaterials;
        GO = go;

        ArraySet(mapArray, size, GO);
    }

    Material MatSet(int R, int C)
    {
        //Place Holder Randomization
        float Rnd = Random.Range(0f, 30f);

        if (Rnd < 2f)
        {
            return dictVisibleMaterials["Mtn"];
        }
        else if (Rnd >= 2f && Rnd < 25f)
        {
            return dictVisibleMaterials["Wtr"];
        }
        else
        {
            return dictVisibleMaterials["Lnd"];
        }
    }

    void ArraySet(Cell[,] map, int size, GameObject gameObject)
    {
        for (int Row = 0; Row < size; Row++)
        {
            for (int Col = 0; Col < size; Col++)
            {
                map[Row, Col] = new Cell(); //Create
                map[Row, Col].plane = GameObject.CreatePrimitive(PrimitiveType.Quad); //Create visible plane
                map[Row, Col].plane.transform.position = new Vector3(Row - (size / 2), 0, Col - (size / 2)); //Offset
                map[Row, Col].plane.transform.rotation = Quaternion.Euler(90, 0, 180); //Set 
                map[Row, Col].plane.AddComponent<CellManager>();
                map[Row, Col].plane.GetComponent<CellManager>().X = Row;
                map[Row, Col].plane.GetComponent<CellManager>().Z = Col;
                map[Row, Col].plane.name = "X" + Row + "Z" + Col;
                map[Row, Col].plane.GetComponent<Collider>().tag = "tile";
                map[Row, Col].plane.transform.parent = gameObject.transform;
                map[Row, Col].isHomeBase = false;
                map[Row, Col].Z = Col;
                map[Row, Col].X = Row;
                map[Row, Col].plane.GetComponent<Renderer>().material = MatSet(Row, Col); // Set Material
            }
        }

        for (int row = 0; row < size; row++)
        {
            for (int col = 0; col < size; col++)
            {
                if (mapArray[row, col].plane.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["Wtr"])
                {
                    pathGrid[row, col] = PathFinderHelper.EMPTY_TILE;
                }
                else
                {
                    pathGrid[row, col] = PathFinderHelper.BLOCKED_TILE;
                }
            }
        }
    }

    public void ShipSet(Game game)
    {
        int[] rowSpawn = new int[] { 0, 17, 3, 14, 6, 11, 9 };

        foreach (Player p in game.players)
        {
            int temp = 0;

            for (int seek = 0; seek < 18; seek++)
            {
                if (p.num % 2 == 0)
                {
                    temp = -seek + 17;
                }
                else
                {
                    temp = seek;
                }
                
                if (mapArray[temp, rowSpawn[p.num - 1]].plane.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["Wtr"])
                {
                    mapArray[temp, rowSpawn[p.num - 1]].plane.GetComponent<Renderer>().material = (p.num == 1) ? dictVisibleMaterials["shpWtr"] : dictVisibleMaterials["shpWtrP2"];
                    mapArray[temp, rowSpawn[p.num - 1]].Occupy = p.Navy[0];
                    mapArray[temp, rowSpawn[p.num - 1]].Owner = p;
                    p.Navy[0].Coordinates = new int[] { temp, rowSpawn[p.num - 1] };
                    p.fogExceptionAdd(p.Navy[0].Coordinates);
                    pathGrid[temp, rowSpawn[p.num - 1]] = PathFinderHelper.BLOCKED_TILE;

                    #region
                    try
                    {
                        p.FogOfWar[p.Navy[0].Coordinates[0], p.Navy[0].Coordinates[1]].plane.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    catch { }
                    try
                    {
                        p.FogOfWar[p.Navy[0].Coordinates[0] + 1, p.Navy[0].Coordinates[1] + 1].plane.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    catch { }
                    try
                    {
                        p.FogOfWar[p.Navy[0].Coordinates[0] - 1, p.Navy[0].Coordinates[1] + 1].plane.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    catch { }
                    try
                    {
                        p.FogOfWar[p.Navy[0].Coordinates[0] + 1, p.Navy[0].Coordinates[1] - 1].plane.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    catch { }
                    try
                    {
                        p.FogOfWar[p.Navy[0].Coordinates[0], p.Navy[0].Coordinates[1] + 1].plane.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    catch { }
                    try
                    {
                        p.FogOfWar[p.Navy[0].Coordinates[0] + 1, p.Navy[0].Coordinates[1]].plane.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    catch { }
                    try
                    {
                        p.FogOfWar[p.Navy[0].Coordinates[0] - 1, p.Navy[0].Coordinates[1] - 1].plane.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    catch { }
                    try
                    {
                        p.FogOfWar[p.Navy[0].Coordinates[0], p.Navy[0].Coordinates[1] - 1].plane.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    catch { }
                    try
                    {
                        p.FogOfWar[p.Navy[0].Coordinates[0] - 1, p.Navy[0].Coordinates[1]].plane.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 0);
                    }
                    catch { }
                    #endregion
                    break;
                }
            }
        }
    }

    public Material GetPlayerShipMaterial(int plyrNum, bool isIlluminated)
    {
        Material shipMat = dictIllumMaterials["IshpWtr"];

        switch (plyrNum)
        {
            case 1:
                {
                    shipMat = isIlluminated ? dictIllumMaterials["IshpWtr"] : dictVisibleMaterials["shpWtr"];
                }
                break;
            case 2:
                {
                    shipMat = isIlluminated ? dictIllumMaterials["IshpWtrP2"] : dictVisibleMaterials["shpWtrP2"];
                }
                break;
            case 3:
                {
                    shipMat = isIlluminated ? dictIllumMaterials["IshpWtrP3"] : dictVisibleMaterials["shpWtrP3"];
                }
                break;
            case 4:
                {
                    shipMat = isIlluminated ? dictIllumMaterials["IshpWtrP4"] : dictVisibleMaterials["shpWtrP4"];
                }
                break;
        }

        return shipMat;
    }

    public void SwitchPlayerFog(int plyrNum, List<Player> players, bool changePlayer)
    {
        Cell[,] fogOn = new Cell[,] { { } }, 
                fogOff = new Cell[,] { { } };

        #region Player switching to
        switch (plyrNum)
        {
            case 1:
                fogOn = players[0].FogOfWar;
                break;
            case 2:
                fogOn = players[1].FogOfWar;
                break;
            case 3:
                fogOn = players[2].FogOfWar;
                break;
            case 4:
                fogOn = players[3].FogOfWar;
                break;
        }
        #endregion

        #region Enable/Disable player fog
        Color32 fogColor = new Color32(165, 165, 165, 255);
        for (int Row = 0; Row < size; Row++)
        {
            for (int Col = 0; Col < size; Col++)
            {
                #region Reset all other players fog
                if (changePlayer)
                {
                    foreach (Player p in players)
                    {
                        if (p.num != plyrNum)
                        {
                            p.FogOfWar[Row, Col].plane.GetComponent<Renderer>().enabled = false;
                        }
                    }
                }
                #endregion

                Renderer enabling = fogOn[Row, Col].plane.GetComponent<Renderer>();
                enabling.enabled = true;
                enabling.material.color = fogColor;
            }
        }
        #endregion

        #region Alter FogOfWar for the active player
        List<int[]> Coordinates = new List<int[]>();
        foreach (string coords in players[plyrNum - 1].fogExceptions)
        {
            string[] splitCoords = coords.Split(',');
            Coordinates.Add(new int[] { int.Parse(splitCoords[0]), int.Parse(splitCoords[1]) });
        }

        RemoveFog(fogOn, Coordinates);
        #endregion
    }

    public void HandlePlayerFog(Cell[,] FogOfWar, List<string> FogExceptions, int[] NewCoords, int[] OldCoords = null)
    {
        List<int[]> lstTurnOff = GetDirectionalCoordinatesDict(NewCoords);
        RemoveFog(FogOfWar, lstTurnOff);
        
        if (OldCoords != null)
        {
            List<int[]> lstTurnOn = GetDirectionalCoordinatesDict(OldCoords);
            EnableFog(FogOfWar, FogExceptions, lstTurnOn);
        }
    }

    public List<int[]> GetDirectionalCoordinatesDict(int[] Coords)
    {
        int X = Coords[0], Z = Coords[1];

        return GetDirectionalCoordinatesDict(X, Z);
    }
    public List<int[]> GetDirectionalCoordinatesDict(int X, int Z)
    {
        List<int[]> dictDirectionalCoords = new List<int[]>();

        dictDirectionalCoords.Add(new int[] { X, Z });     //Current
        dictDirectionalCoords.Add(new int[] { X, Z + 1 });     //North
        dictDirectionalCoords.Add(new int[] { X + 1, Z + 1 }); //NorthEast
        dictDirectionalCoords.Add(new int[] { X + 1, Z });     //East
        dictDirectionalCoords.Add(new int[] { X + 1, Z - 1 }); //SouthEast
        dictDirectionalCoords.Add(new int[] { X, Z - 1 });     //South
        dictDirectionalCoords.Add(new int[] { X - 1, Z - 1 }); //SouthWest
        dictDirectionalCoords.Add(new int[] { X - 1, Z });     //West
        dictDirectionalCoords.Add(new int[] { X - 1, Z + 1 }); //NorthWest

        return dictDirectionalCoords;
    }

    public void RemoveFog(Cell[,] FogOfWar, List<int[]> lstCoordinates)
    {
        Color32 hideFog = new Color32(0, 0, 0, 0);
        foreach (int[] coords in lstCoordinates)
        {
            int X = coords[0], Z = coords[1];
            if (IsOnMap(X, Z))
            {
                Renderer hiding = FogOfWar[X, Z].plane.GetComponent<Renderer>();
                hiding.material = GetFogMaterial(X, Z);
                hiding.material.color = hideFog;
            }
        }
    }
    public void EnableFog(Cell[,] FogOfWar, List<string> FogExceptions, List<int[]> DirectionalCoordinates)
    {
        Color32 hideFog = new Color32(165, 165, 165, 255);
        foreach (int[] coords in DirectionalCoordinates)
        {
            int X = coords[0], Z = coords[1];
            if (IsOnMap(X, Z) && !FogExceptions.Contains(coords[0] + "," + coords[1]))
            {
                FogOfWar[X, Z].plane.GetComponent<Renderer>().material.color = hideFog;
            }
        }
    }

    public Material GetFogMaterial(int X, int Z)
    {
        Material fogMaterial = dictFogMaterials["fogWtr"];

        if (mapArray[X, Z].plane.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["Wtr"])
        {
            //Continue
        }
        else if(mapArray[X, Z].plane.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["Lnd"])
        {
            fogMaterial = dictFogMaterials["fogLnd"];
        }
        else if (mapArray[X, Z].plane.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["Mtn"])
        {
            fogMaterial = dictFogMaterials["fogMtn"];
        }
        else if (mapArray[X, Z].plane.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["homeBase"])
        {
            fogMaterial = dictFogMaterials["fogHome"];
        }

        return fogMaterial;
    }

    public bool IsOnMap(int X, int Z)
    {
        if ((X <= size - 1 && X >= 0) && (Z <= size - 1 && Z >= 0))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
#endregion Map

#region Movement Direction Names
public static class Dir
{
    public static string North = "North";
    public static string NorthEast = "NorthEast";
    public static string East = "East";
    public static string SouthEast = "SouthEast";
    public static string South = "South";
    public static string SouthWest = "SouthWest";
    public static string West = "West";
    public static string NorthWest = "NorthWest"; 
}
#endregion

public class MapManager : MonoBehaviour
{
    #region Members
    [HideInInspector]
    public int xPos, zPos, xTilePos, zTilePos;
    public int MapSize = 18;
    public bool isHomeTileSelected = false, holdState;
    public Map Main;
    public Fleet selectedFleet;
    public HomeBase selectedHomeBase;
    public battleManager currentBattle;
    public Collider tableCollider;
    public Game mainGame;
    public GameManager gameMan;

    public Collider previousTileCollider, previousShipHomeCollider;
    public Material previousTileMat, previousShipHomeMat;

    Ray ray;
    RaycastHit hit;
    Vector3 fingerStart, fingerEnd;
    int currentFog;
    bool created = false, firstClick = true;
    
    public Dictionary<string, Material> dictVisibleMaterials = new Dictionary<string, Material>();
    public Dictionary<string, Material> dictIllumMaterials = new Dictionary<string, Material>();
    public Dictionary<string, Material> dictFogMaterials = new Dictionary<string, Material>();
    #endregion Members

    #region Start / Update
    void Start()
    {
        #region Initialize Material Dictionaries
        //Visible Tile materials
        dictVisibleMaterials.Add("Lnd",      Resources.Load("Materials/Visible/Lnd") as Material);
        dictVisibleMaterials.Add("Wtr",      Resources.Load("Materials/Visible/Wtr") as Material);
        dictVisibleMaterials.Add("Mtn",      Resources.Load("Materials/Visible/Mtn") as Material);
        dictVisibleMaterials.Add("shpWtr",   Resources.Load("Materials/Visible/shpWtr") as Material);
        dictVisibleMaterials.Add("shpWtrP2", Resources.Load("Materials/Visible/shpWtrP2") as Material);
        dictVisibleMaterials.Add("fltWtr",   Resources.Load("Materials/Visible/FltWtr") as Material);
        dictVisibleMaterials.Add("homeBase", Resources.Load("Materials/Visible/homeBase") as Material);

        //Illuminated Tile materials
        dictIllumMaterials.Add("IWtr", Resources.Load("Materials/IllumVisible/IWtr") as Material);
        dictIllumMaterials.Add("IMtn", Resources.Load("Materials/IllumVisible/IMtn") as Material);
        dictIllumMaterials.Add("ILnd", Resources.Load("Materials/IllumVisible/ILnd") as Material);
        dictIllumMaterials.Add("IshpWtr", Resources.Load("Materials/IllumVisible/IshpWtr") as Material);
        dictIllumMaterials.Add("IshpWtrP2", Resources.Load("Materials/IllumVisible/IshpWtrP2") as Material);
        dictIllumMaterials.Add("IhomeBase", Resources.Load("Materials/IllumVisible/IhomeBase") as Material);

        //Fog Tile materials
        dictFogMaterials.Add("fogWtr", Resources.Load("Materials/Fog/fogWtr") as Material);
        dictFogMaterials.Add("fogLnd", Resources.Load("Materials/Fog/fogLnd") as Material);
        dictFogMaterials.Add("fogMtn", Resources.Load("Materials/Fog/fogMtn") as Material);
        dictFogMaterials.Add("fogHome", Resources.Load("Materials/Fog/fogHomeBase") as Material);
        dictFogMaterials.Add("cloudFog", Resources.Load("Materials/Fog/cloudFog") as Material);
        #endregion

        gameMan = GameObject.Find("gameManager").GetComponent<GameManager>();
        mainGame = gameMan.gameMan; 
        currentBattle = GameObject.Find("gameManager").GetComponent<battleManager>();
    }
    
    void Update()
    {
        #region Generate and handle map
        if (created == false)
        { 
            Main = new Map(MapSize, dictVisibleMaterials, dictIllumMaterials, dictFogMaterials, gameObject);
            
            if (mainGame == null)
            {
                gameMan.gameMan = new Game(Main.size, 2);
                mainGame = gameMan.gameMan;
                gameMan.gameMan.round = 1;
                Main.ShipSet(mainGame);
                Main.SwitchPlayerFog(mainGame.getPlayerNum(), mainGame.players, false);
            }

            firstClick = true;
            created = true;
        }
        #endregion

        #region TEST

        if (Input.touchCount > 0)
        {
            Touch mytouch = Input.GetTouch(0);
        }

        if (Input.GetMouseButtonDown(0))
        {
            fingerStart = Input.mousePosition;
            fingerEnd = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (Mathf.Abs(fingerEnd.x - fingerStart.x) < 0.5f && Mathf.Abs(fingerEnd.y - fingerStart.y) < 0.1f)
            {
                Illuminate();
            }

            fingerStart = Vector2.zero;
            fingerEnd = Vector2.zero;
        }

        if (Input.GetMouseButtonUp(1))
        {
            if (selectedFleet != null)
            {
                ClickMove();
            }
        }
        #endregion TEST
    }
    #endregion Start / Update

    #region MapManager Methods - (Many need to be put in respective classes)
    void Illuminate()
    {
        // Reset ray with new mouse position
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            Collider collider = hit.collider;
            if (collider.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["shpWtr"] || collider.GetComponent<Renderer>().sharedMaterial == dictIllumMaterials["IshpWtr"]
                || collider.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["shpWtrP2"] || collider.GetComponent<Renderer>().sharedMaterial == dictIllumMaterials["IshpWtrP2"]
                || collider.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["homeBase"] || collider.GetComponent<Renderer>().sharedMaterial == dictIllumMaterials["IhomeBase"])
            {
                xPos = collider.gameObject.GetComponent<CellManager>().X;
                zPos = collider.gameObject.GetComponent<CellManager>().Z;
                ClickedShipHome(collider);
            }
            else if(collider.tag == "tile")
            {
                ClickedTile(collider);
            }
            firstClick  = false;
        }

        // Draw a red line from camera to selected object in Scene window
        Debug.DrawLine(ray.origin, hit.point, Color.red);
    }

    void ClickMove()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["Wtr"])
            {
                CellManager cell = hit.collider.gameObject.GetComponent<CellManager>();

                List<PathFinderNode> path = new PathFinder(Main.pathGrid).FindPath(new Point(selectedFleet.Coordinates[0], selectedFleet.Coordinates[1]), new Point(cell.X, cell.Z));
                StartCoroutine(MoveAlongPath(path));
            }
        }
    }

    public void ClickedShipHome(Collider collider)
    {
        if (!firstClick && previousShipHomeCollider != null)
        {
            previousShipHomeCollider.GetComponent<Renderer>().material = previousShipHomeMat;
            previousShipHomeCollider.gameObject.GetComponent<CellManager>().hit = false;
        }

        if (Main.mapArray[xPos, zPos].Occupy != null && Main.mapArray[xPos, zPos].Occupy.ownerNum == mainGame.pTurn)
        {
            selectedHomeBase = null;
            selectedFleet = Main.mapArray[xPos, zPos].Occupy;
        }
        else if (Main.mapArray[xPos, zPos].homeBase != null && Main.mapArray[xPos, zPos].homeBase.Owner.num == mainGame.pTurn)
        {
            selectedFleet = null;
            selectedHomeBase = Main.mapArray[xPos, zPos].homeBase;
        }

        previousShipHomeCollider = collider;
        
        Material ClickedMaterial = collider.GetComponent<Renderer>().sharedMaterial;
        if (ClickedMaterial == dictVisibleMaterials["shpWtr"])
        {
            collider.GetComponent<Renderer>().sharedMaterial = Main.GetPlayerShipMaterial(1, true);
            previousShipHomeMat = Main.GetPlayerShipMaterial(1, false);
        }
        else if (ClickedMaterial == dictIllumMaterials["IshpWtr"])
        {
            collider.GetComponent<Renderer>().material = Main.GetPlayerShipMaterial(1, false);
            previousShipHomeMat = Main.GetPlayerShipMaterial(1, true);
        }
        else if (ClickedMaterial == dictVisibleMaterials["shpWtrP2"])
        {
            collider.GetComponent<Renderer>().material = Main.GetPlayerShipMaterial(2, true);
            previousShipHomeMat = Main.GetPlayerShipMaterial(2, false);
        }
        else if (ClickedMaterial == dictIllumMaterials["IshpWtrP2"])
        {
            collider.GetComponent<Renderer>().material = Main.GetPlayerShipMaterial(2, false);
            previousShipHomeMat = Main.GetPlayerShipMaterial(2, true);
        }
        else if (ClickedMaterial == dictVisibleMaterials["homeBase"])
        {
            collider.GetComponent<Renderer>().material = dictIllumMaterials["IhomeBase"];
            previousShipHomeMat = dictVisibleMaterials["homeBase"];
        }
    }

    public void ClickedTile(Collider collider)
    {
        Material ClickedMaterial = collider.GetComponent<Renderer>().sharedMaterial;

        #region Material is a Visible matrerial
        if (collider.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["Wtr"])
        {
            ResetTile(collider);
            collider.GetComponent<Renderer>().material = dictIllumMaterials["IWtr"];
            previousTileMat = dictVisibleMaterials["Wtr"];
        }
        else if (ClickedMaterial == dictVisibleMaterials["Lnd"])
        {
            ResetTile(collider);
            collider.GetComponent<Renderer>().material = dictIllumMaterials["ILnd"];
            previousTileMat = dictVisibleMaterials["Lnd"];
        }
        else if (ClickedMaterial == dictVisibleMaterials["Mtn"])
        {
            ResetTile(collider);
            collider.GetComponent<Renderer>().material = dictIllumMaterials["IMtn"];
            previousTileMat = dictVisibleMaterials["Mtn"];
        }
        #endregion

        #region Material is an Illuminated material        
        else if (ClickedMaterial == dictIllumMaterials["IWtr"])
        {
            collider.GetComponent<Renderer>().material = dictVisibleMaterials["Wtr"];
            previousTileCollider = null;
            previousTileMat = null;
        }
        else if (ClickedMaterial == dictIllumMaterials["ILnd"])
        {
            collider.GetComponent<Renderer>().material = dictVisibleMaterials["Lnd"];
            previousTileCollider = null;
        }
        else if (ClickedMaterial == dictIllumMaterials["IMtn"])
        {
            collider.GetComponent<Renderer>().material = dictVisibleMaterials["Mtn"];
            previousTileCollider = null;
        }
        #endregion
    }

    public void ResetTile(Collider collider)
    {
        if (previousTileCollider != null)
        {
            previousTileCollider.GetComponent<Renderer>().material = previousTileMat;
            previousTileCollider.gameObject.GetComponent<CellManager>().hit = false;
        }

        previousTileCollider = collider;
    }

    IEnumerator MoveAlongPath(List<PathFinderNode> resultPathList)
    {
        foreach (PathFinderNode position in resultPathList)
        {
            selectedFleet.MoveOperations(new int[] { position.X, position.Y }, this);

            if (selectedFleet.moveCount > 0)
            {
                yield return new WaitForSeconds(.2f);
            }
            else
            {
                yield break;
            }
        }
    }

    public void Movement(int move)
    {
        Dictionary<string, int[]> DirectionalCoordinates = selectedFleet.GetDirectionalCoordinatesDict();
        int[] Coords = selectedFleet.Coordinates;

        if (selectedFleet.moveCount > 0)
        {
            switch (move)
            {
                case 0:
                    break;

                case 1:
                    // north
                    selectedFleet.MoveOperations(DirectionalCoordinates[Dir.North], this);
                    break;

                case 2:
                    // northe east
                    selectedFleet.MoveOperations(DirectionalCoordinates[Dir.NorthEast], this);
                    break;

                case 3:
                    // east
                    selectedFleet.MoveOperations(DirectionalCoordinates[Dir.East], this);
                    break;

                case 4:
                    // south east
                    selectedFleet.MoveOperations(DirectionalCoordinates[Dir.SouthEast], this);
                    break;

                case 5:
                    // south
                    selectedFleet.MoveOperations(DirectionalCoordinates[Dir.South], this);
                    break;

                case 6:
                    // south west
                    selectedFleet.MoveOperations(DirectionalCoordinates[Dir.SouthWest], this);
                    break;

                case 7:
                    // west
                    selectedFleet.MoveOperations(DirectionalCoordinates[Dir.West], this);
                    break;

                case 8:
                    // north west
                    selectedFleet.MoveOperations(DirectionalCoordinates[Dir.NorthWest], this);
                    break;
            }
        }
    }
    #endregion MapManager Methods

    #region OLD

    //public void ClickOperations(Collider collider, bool firstClick, bool newFleet = false)
    //{
    //    if (firstClick)
    //    {
    //        #region
    //        /*
    //            -Tell The Collider It is hit.
    //            -Set X/Z Coords for the Map Manager
    //            -Skip First Click operations next time
    //            -if the cell selected has a fleet and it belongs to the current player
    //                -Set The mapManagers selectedFleet to the selected cells occupying fleet
    //            -else the map managers selectedFleet is NULL
    //        */
    //        collider.gameObject.GetComponent<CellManager>().hit = true;

    //        if (collider.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["shpWtr"] || collider.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["homeBase"])
    //        {
    //            xPos = collider.gameObject.GetComponent<CellManager>().X;
    //            zPos = collider.gameObject.GetComponent<CellManager>().Z;
    //            ClickedShipHome(collider);
    //        }
    //        else
    //        {
    //            ClickedTile(collider);
    //        }
    //        //Cache the previous collider to reset the previous cell after a new selection
    //        #endregion
    //    }
    //    else if (collider.tag != "noillumblock" || newFleet)
    //    {
    //        #region 
    //        //Same as first click except the previous cell is reset.
    //        if (collider.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["shpWtr"] || collider.GetComponent<Renderer>().sharedMaterial == dictIllumMaterials["IshpWtr"]
    //            || collider.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["shpWtrP2"] || collider.GetComponent<Renderer>().sharedMaterial == dictIllumMaterials["IshpWtrP2"]
    //            || collider.GetComponent<Renderer>().sharedMaterial == dictVisibleMaterials["homeBase"] || collider.GetComponent<Renderer>().sharedMaterial == dictIllumMaterials["IhomeBase"])
    //        {
    //            xPos = collider.gameObject.GetComponent<CellManager>().X;
    //            zPos = collider.gameObject.GetComponent<CellManager>().Z;
    //            ClickedShipHome(collider);
    //        }
    //        else
    //        {
    //            ClickedTile(collider);
    //        }
    //        #endregion
    //    }
    //}

    #endregion
}
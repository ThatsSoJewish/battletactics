﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour
{
    public void LoadMapScreen()
    {
        SceneManager.LoadScene("MapScreen");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}

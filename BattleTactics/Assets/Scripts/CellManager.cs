﻿using UnityEngine;
using System.Collections;

public class CellManager : MonoBehaviour
{
    #region Members

    public Material current { get; set; }
    public Material previous { get; set; }
    public int X { get; set; }
    public int Z { get; set; }
    public bool hit { get; set; }

    #endregion Members

    #region Start / Update

    void Start()
    {
        hit = false;
    }

    void Update()
    {

    }

#endregion Start / Update
}
﻿using UnityEngine;
using System.Collections;

public class cameraScroll : MonoBehaviour
{
    #region Members

    public float speed { get; set; }
    GUIManager guiMan { get; set; }
    Transform mainCam { get; set; }
    Vector3 CamPos { get; set; }
    public bool isPCMode = true;

    #endregion Members

    #region Start / Update

    void Start()
    {
        speed = 0.03f;
        mainCam = gameObject.transform;
        guiMan = GameObject.Find("mapMain").GetComponent<GUIManager>();
        CamPos = mainCam.position;

        if (isPCMode)
        {
            mainCam.position = new Vector3(CamPos.x, 65, -0.5f);
        }
    }

    void Update()
    {
        #region Zoomed out (2 distances for testing)

        if (Input.touchCount > 0 && (mainCam.position.y == 80 || mainCam.position.y == 45))
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began || Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

                if (mainCam.position.z < 3.75 && mainCam.position.z > -4.75f)
                {
                    mainCam.Translate(0, Mathf.Clamp(-touchDeltaPosition.y * speed, -1, 1), 0);
                }
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                if (mainCam.position.z >= 3.75)
                {
                    mainCam.position = new Vector3(-0.5f, mainCam.position.y, 3.74f);
                }
                else if (mainCam.position.z <= -4.75f)
                {
                    mainCam.position = new Vector3(-0.5f, mainCam.position.y, -4.74f);
                }

                if (mainCam.position.x >= 5 || mainCam.position.x <= -6)
                {
                    mainCam.position = new Vector3(0.5f, mainCam.position.y, -4.75f);
                }
            }
        }

        #endregion Zoomed out (2 distances for testing)
        //These 2 regions are separated by an 'else if'
        #region Zoomed in 

        else if (Input.touchCount > 0 && CamPos.y == 20)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began || Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

                if (CamPos.z < 6.5 && CamPos.z > -7.5f && CamPos.x < 5.5 && CamPos.x > -6.5f)
                {
                    mainCam.Translate(Mathf.Clamp(-touchDeltaPosition.x * speed, -1, 1), Mathf.Clamp(-touchDeltaPosition.y * speed, -1, 1), 0);
                }
                else if (CamPos.z < 6.5 && CamPos.z > -7.5f)
                {
                    mainCam.Translate(0, Mathf.Clamp(-touchDeltaPosition.y * speed, -1, 1), 0);
                }
                else if (CamPos.x < 5.5 && CamPos.x > -6.5f)
                {
                    mainCam.Translate(Mathf.Clamp(-touchDeltaPosition.x * speed, -1, 1), 0, 0);
                }
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                if (CamPos.z >= 6.5)
                    CamPos = new Vector3(CamPos.x, CamPos.y, 6.4f);
                else
                    if (CamPos.z <= -7.5f)
                    CamPos = new Vector3(CamPos.x, CamPos.y, -7.4f);

                if (CamPos.x >= 5.5)
                    CamPos = new Vector3(5.4f, CamPos.y, CamPos.z);
                else
                    if (CamPos.x <= -6.5)
                    CamPos = new Vector3(-6.4f, CamPos.y, CamPos.z);
            }
        }

        #endregion Zoomed in 

        #region PC controls (disabled)

        //float z;
        //if (Input.GetAxis("Mouse ScrollWheel") < 0)
        //{
        //    z = (float)gameObject.transform.position.z - 0.5f;
        //    gameObject.transform.position = new Vector3(-0.5f, 30f, Mathf.Clamp(z, -6f, 5f));
        //}
        //else if (Input.GetAxis("Mouse ScrollWheel") > 0)
        //{
        //    z = (float)gameObject.transform.position.z + 0.5f;
        //    gameObject.transform.position = new Vector3(-0.5f, 30f, Mathf.Clamp(z, -6f, 5f));
        //}

        #endregion PC controls (disabled)
    }

    #endregion Start / Update
}

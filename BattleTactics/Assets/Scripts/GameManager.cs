﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AStarPathFinder;

#region Ship Base & Types
public abstract class Ship
{
    public int X { get; set; }
    public int Z { get; set; }
    public int moves { get; set; }
    public float health { get; set; }
    public float strength { get; set; }
    public float hull { get; set; }
    public string type { get; set; }

    public Ship()
    {
        health = 100;
    }
}

public class battleShip : Ship
{
    public battleShip()
    {
        strength = 80;
        hull = 60;
        type = "BattleShip";
        moves = 1;
    }
}

public class carrier : Ship
{
    public carrier()
    {
        strength = 55;
        hull = 40;
        type = "Aircraft Carrier";
        moves = 1;
    }
}

public class cruiser : Ship
{
    public cruiser()
    {
        strength = 35;
        hull = 35;
        type = "Battle Cruiser";
        moves = 2;
    }
}

public class scout : Ship
{
    public scout()
    {
        strength = 20;
        hull = 15;
        type = "Scout Ship";
        moves = 25;
    }
}

public class cargoShip : Ship
{
    public cargoShip()
    {
        strength = 10;
        hull = 40;
        type = "Cargo Vessel";
        moves = 2;
    }
}

public class submarine : Ship
{
    public submarine()
    {
        strength = 65;
        hull = 35;
        type = "Submarine";
        moves = 2;
    }
}

#endregion Ship Types

#region Generals
public class General
{
    public int level { get; set; }
    public string name { get; set; }
}
#endregion Generals

#region Fleet
public class Fleet
{
    public List<Ship> Flt { get; set; }
    public Player Owner { get; set; }
    public General general { get; set; }
    public HomeBase homeBase { get; set; }

    int X;
    int Z;
    public int[] Coordinates
    {
        get
        {
            return new int[] { X, Z };
        }
        set
        {
            int[] coords = value as int[];
            X = coords[0];
            Z = coords[1];
        }
    }

    public int moveCount { get; set; }
    public int move { get; set; }
    public int ownerNum { get; set; }
    public int tileType { get; set; }

    public double supply { get; set; }
    public double supplyRate { get; set; }

    public Fleet(Player owner, bool scout, HomeBase home = null)
    {
        supplyRate = 5;
        homeBase = home;
        tileType = 0;
        Owner = owner;
        ownerNum = owner.num;
        supply = 5;
        Flt = new List<Ship>();

        if (scout)
        {
            Flt.Add(new scout());
            SetMove(true);
        }
    }

    public void SetMove(bool setNow)
    {
        move = Flt[0].moves;

        foreach (Ship shp in Flt)
        {
            if (move > shp.moves)
            {
                move = shp.moves;
            }
        }

        if (setNow)
            moveCount = move;
    }

    public void AdjustMoveCount()
    {
        if (moveCount > 0)
            moveCount--;
    }

    public void Resupply()
    {
        if (moveCount > 0)
        {
            if (homeBase.supplies >= supplyRate)
            {
                supply += supplyRate;
                homeBase.supplies -= supplyRate;
                moveCount = 0;
            }
            else if (homeBase.supplies > 0)
            {
                supply += homeBase.supplies;
                homeBase.supplies = 0;
                moveCount = 0;
            }
        }
    }

    public void HomeBaseOperations(int[] HomeBaseCoords, MapManager mapMan)//(int X, int Z, MapManager mapMan)
    {
        int _X = HomeBaseCoords[0], _Z = HomeBaseCoords[1];

        Cell[,] map = mapMan.Main.mapArray;

        if (Owner.CanCreateNewBase() && Owner.homeBasePrices[Owner.nextHomeBase] <= supply && map[_X, _Z].plane.GetComponent<Renderer>().sharedMaterial == mapMan.dictVisibleMaterials["Lnd"])
        {
            Owner.HomeBase[Owner.nextHomeBase] = map[_X, _Z];
            map[_X, _Z].plane.GetComponent<Renderer>().material = mapMan.dictVisibleMaterials["homeBase"];
            map[_X, _Z].isHomeBase = true;
            map[_X, _Z].Owner = Owner;
            map[_X, _Z].homeBase = new HomeBase(_X, _Z, Owner);
            Owner.fogExceptionAdd(new int[] { _X, _Z });
            mapMan.Main.HandlePlayerFog(Owner.FogOfWar, Owner.fogExceptions, new int[] { _X, _Z });
            this.homeBase = map[_X, _Z].homeBase;
            this.moveCount = 0;
            this.supply -= Owner.homeBasePrices[Owner.nextHomeBase];
            Owner.nextHomeBase += 1;
        }
    }

    public void HealFleet()
    {
        foreach (Ship shp in Flt)
        {
            if (moveCount > 0)
            {
                if (shp.health > 80 && shp.health < 100)
                {
                    shp.health = 100f;
                    moveCount = 0;
                }
                else if (shp.health <= 80)
                {
                    shp.health += 20;
                    moveCount = 0;
                }
            }
        }
    }

    public void addShip(int shpChoice, bool fromHomeBase = false)
    {
        switch (shpChoice)
        {
            case 0:
                //BattleShip
                Flt.Add(new battleShip());
                break;

            case 1:
                //Cruiser
                Flt.Add(new cruiser());
                break;

            case 2:
                //Scout
                Flt.Add(new scout());
                break;

            case 3:
                //Carrier
                Flt.Add(new carrier());
                break;

            case 4:
                //Submarine
                Flt.Add(new submarine());
                break;
        }

        SetMove(false);
    }

    public void MoveOperations(int[] MovedToCoords, MapManager mapMan)
    {
        int _X = MovedToCoords[0], _Z = MovedToCoords[1];

        Debug.Log(_X + " , " + _Z);

        if (mapMan.Main.mapArray[_X, _Z].plane.GetComponent<Renderer>().sharedMaterial == mapMan.dictVisibleMaterials["Wtr"] && mapMan.Main.IsOnMap(_X, _Z))
        {
            Owner.fogExceptionRemove(new int[] { X, Z });
            mapMan.Main.pathGrid[X, Z] = PathFinderHelper.EMPTY_TILE;
            Owner.fogExceptionAdd(new int[] { _X, _Z });
            mapMan.Main.pathGrid[X, Z] = PathFinderHelper.BLOCKED_TILE;
            mapMan.Main.HandlePlayerFog(Owner.FogOfWar, Owner.fogExceptions, new int[] { _X, _Z }, new int[] { X, Z });
            
            //Reset Old Cell
            mapMan.Main.mapArray[X, Z].plane.GetComponent<Renderer>().material = mapMan.dictVisibleMaterials["Wtr"];
            mapMan.Main.mapArray[X, Z].Occupy = null;
            mapMan.Main.mapArray[X, Z].Owner = null;
            
            //Set New Cell
            mapMan.Main.mapArray[_X, _Z].plane.GetComponent<Renderer>().material = mapMan.Main.GetPlayerShipMaterial(ownerNum, true);
            mapMan.Main.mapArray[_X, _Z].Occupy = this;
            mapMan.Main.mapArray[_X, _Z].Owner = Owner;
            mapMan.xPos = _X;
            mapMan.zPos = _Z;

            //Set new X/Z on selected fleet
            X = _X;
            Z = _Z;
            mapMan.previousShipHomeCollider = mapMan.Main.mapArray[_X, _Z].plane.GetComponent<Collider>();
            AdjustMoveCount();
        }
        //Battle Conditions
        else if ((mapMan.Main.mapArray[_X, _Z].plane.GetComponent<Renderer>().sharedMaterial == mapMan.dictVisibleMaterials["shpWtr"]
                || mapMan.Main.mapArray[_X, _Z].plane.GetComponent<Renderer>().sharedMaterial == mapMan.dictVisibleMaterials["shpWtrP2"])
                    && mapMan.Main.mapArray[_X, _Z].Owner.num != ownerNum)
        {
            mapMan.currentBattle.startBattle(this, mapMan.Main.mapArray[_X, _Z].Occupy);
        }
    }

    public Dictionary<string, int[]> GetDirectionalCoordinatesDict()
    {
        Dictionary<string, int[]> dictDirectionalCoords = new Dictionary<string, int[]>();
        
        dictDirectionalCoords.Add("North",     new int[] { X,     Z + 1 }); //North
        dictDirectionalCoords.Add("NorthEast", new int[] { X + 1, Z + 1 }); //NorthEast
        dictDirectionalCoords.Add("East",      new int[] { X + 1, Z });     //East
        dictDirectionalCoords.Add("SouthEast", new int[] { X + 1, Z - 1 }); //SouthEast
        dictDirectionalCoords.Add("South",     new int[] { X,     Z - 1 }); //South
        dictDirectionalCoords.Add("SouthWest", new int[] { X - 1, Z - 1 }); //SouthWest
        dictDirectionalCoords.Add("West",      new int[] { X - 1, Z });     //West
        dictDirectionalCoords.Add("NorthWest", new int[] { X - 1, Z + 1 }); //NorthWest

        return dictDirectionalCoords;
    }

    /// <summary>
    /// The direction in the list are ordered as follows:
    /// **North, NorthEast, East, SouthEast, South, SouthWest, West, NorthWest**
    /// </summary>
    /// <returns></returns>
    public List<int[]> GetDirectionalCoordinatesList(bool IncludeShipTile = false)
    {
        List<int[]> arrayDirectionalCoords = new List<int[]>();

        arrayDirectionalCoords.Add(new int[] { X, Z + 1 });     //North
        arrayDirectionalCoords.Add(new int[] { X + 1, Z + 1 }); //NorthEast
        arrayDirectionalCoords.Add(new int[] { X + 1, Z });     //East
        arrayDirectionalCoords.Add(new int[] { X + 1, Z - 1 }); //SouthEast
        arrayDirectionalCoords.Add(new int[] { X, Z - 1 });     //South
        arrayDirectionalCoords.Add(new int[] { X - 1, Z - 1 }); //SouthWest
        arrayDirectionalCoords.Add(new int[] { X - 1, Z });     //West
        arrayDirectionalCoords.Add(new int[] { X - 1, Z + 1 }); //NorthWest

        if (IncludeShipTile)
        {
            arrayDirectionalCoords.Add(new int[] { X, Z }); //ShipTile
        }

        return arrayDirectionalCoords;
    }
    
    public void ClaimTile(int claim, MapManager mapMan)
    {
        Dictionary<string, int[]> DirectionalCoordinates = GetDirectionalCoordinatesDict();

        if (moveCount > 0)
        {
            switch (claim)
            {
                case 0:
                    //Do Nothing
                    break;

                case 1:
                    //North
                    HomeBaseOperations(DirectionalCoordinates[Dir.North], mapMan);
                    break;

                case 2:
                    //North-East
                    HomeBaseOperations(DirectionalCoordinates[Dir.NorthEast], mapMan);
                    break;

                case 3:
                    //East
                    HomeBaseOperations(DirectionalCoordinates[Dir.East], mapMan);
                    break;

                case 4:
                    //South-East
                    HomeBaseOperations(DirectionalCoordinates[Dir.SouthEast], mapMan);
                    break;

                case 5:
                    //South
                    HomeBaseOperations(DirectionalCoordinates[Dir.South], mapMan);
                    break;

                case 6:
                    //South-West
                    HomeBaseOperations(DirectionalCoordinates[Dir.SouthWest], mapMan);
                    break;

                case 7:
                    //West
                    HomeBaseOperations(DirectionalCoordinates[Dir.West], mapMan);
                    break;

                case 8:
                    //North-West
                    HomeBaseOperations(DirectionalCoordinates[Dir.NorthWest], mapMan);
                    break;
            }
        }
    }
}
#endregion Fleet

#region Player
public class Player
{
    public List<Fleet> Navy { get; set; }
    public int num { get; set; }
    public int homeBaseLimit { get; set; }
    public int nextHomeBase { get; set; }
    public Cell[] HomeBase { get; set; }
    public Cell[,] FogOfWar { get; set; }
    public int[] homeBasePrices = { 5, 10, 20, 40, 80 };
    public List<string> fogExceptions;

    public Player(int size, int plyrNum)
    {
        num = plyrNum;

        Navy = new List<Fleet>();
        Navy.Add(new Fleet(this, true));
        Navy[0].ownerNum = num;

        HomeBase = new Cell[5] { null, null, null, null, null };
        homeBaseLimit = 1;

        FogOfWar = new Cell[size, size];
        FogSet(FogOfWar, size);
        nextHomeBase = 0;
        fogExceptions = new List<string>();
    }

    public void AddSuppliesToHomeBases()
    {
        foreach (Cell c in HomeBase)
        {
            if (c != null)
            {
                c.homeBase.supplies += c.homeBase.supplyRate;
            }
        }
    }

    public bool CanCreateNewBase()
    {
        //This method will return true when the Player IS able to create a homebase
        //False is returned whent the user has hit the limit of HomeBases
        if (HomeBase[(homeBaseLimit - 1)] == null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void FogSet(Cell[,] Fog, int size)
    {
        Color32 dimmed = new Color32(165, 165, 165, 255);
        Material fogMat = (Material)Resources.Load("Materials/Fog/cloudFog", typeof(Material));
        Transform fogHolder = GameObject.Find("FogHolder").transform;
        Vector3 scaleUp = new Vector3(0.025F, 0.025f, 0);
        Quaternion rotation = Quaternion.Euler(90, 0, 180);
        int Layer = LayerMask.NameToLayer("Ignore Raycast");

        for (int Row = 0; Row < size; Row++)
        {
            for (int Col = 0; Col < size; Col++)
            {
                Fog[Row, Col] = new Cell(); //Create
                Fog[Row, Col].plane = GameObject.CreatePrimitive(PrimitiveType.Quad); //Create visible plane
                Fog[Row, Col].plane.GetComponent<Renderer>().material = fogMat;
                Fog[Row, Col].plane.GetComponent<Renderer>().material.color = dimmed;
                Fog[Row, Col].plane.layer = Layer;
                Fog[Row, Col].plane.transform.position = new Vector3(Row - (size / 2), 0.001f, Col - (size / 2)); //Offset
                Fog[Row, Col].plane.transform.rotation = rotation; //Set 
                Fog[Row, Col].plane.name = "P" + num + "- X" + Row + "Z" + Col;
                Fog[Row, Col].plane.transform.localScale += scaleUp;
                Fog[Row, Col].plane.transform.parent = fogHolder;
                Fog[Row, Col].isHomeBase = false;
                Fog[Row, Col].Z = Col;
                Fog[Row, Col].X = Row;

                if (num != 1)
                {
                    Fog[Row, Col].plane.GetComponent<Renderer>().enabled = false;
                }
            }
        }
    }

    public void fogExceptionAdd(int[] Coords)
    {
        //string strCoords = Coords[0] + "," + Coords[1];
        //fogExceptions.Add(strCoords);

        fogExceptions.AddRange(GetDirectionalStrings(Coords));
    }

    public void fogExceptionRemove(int[] Coords)
    {
        //string strRemove = Coords[0] + "," + Coords[1];
        //fogExceptions.Remove(strRemove);
        List<string> remove = GetDirectionalStrings(Coords);

        foreach (string coords in remove)
        {
            fogExceptions.Remove(coords);
        }
    }

    public List<string> GetDirectionalStrings(int[] Coords)
    {
        List<string> arrayDirectionalCoords = new List<string>();

        int X = Coords[0], Z = Coords[1];
        arrayDirectionalCoords.Add(X + "," + (Z + 1));       //North
        arrayDirectionalCoords.Add((X + 1) + "," + (Z + 1)); //NorthEast
        arrayDirectionalCoords.Add((X + 1) + "," + Z);       //East
        arrayDirectionalCoords.Add((X + 1) + "," + (Z - 1)); //SouthEast
        arrayDirectionalCoords.Add(X + "," + (Z - 1));       //South
        arrayDirectionalCoords.Add((X - 1) + "," + (Z - 1)); //SouthWest
        arrayDirectionalCoords.Add((X - 1) + "," + Z);       //West
        arrayDirectionalCoords.Add((X - 1) + "," + (Z + 1)); //NorthWest
        arrayDirectionalCoords.Add(X + "," + Z);             //ShipTile

        return arrayDirectionalCoords;
    }
}

#endregion Player

#region Game
public class Game
{
    public int NumOfPlayers { get; set; }
    public List<Player> players { get; set; }
    public int pTurn { get; set; }
    public int round { get; set; }

    public Game(int mapSize, int numOfPlayers)
    {
        pTurn = 1;
        players = new List<Player>();

        NumOfPlayers = (numOfPlayers > 4) ? 4 : numOfPlayers;
        for (int plyrs = 0; plyrs < numOfPlayers; plyrs++)
        {
            players.Add(new Player(mapSize, plyrs + 1));
        }
    }
    
    public Player getPlayer()
    {
        return players[pTurn - 1];
    }

    public int getPlayerNum()
    {
        return pTurn;
    }

    public int changePlayer()
    {
        if (pTurn + 1 > NumOfPlayers)
        {
            return 1;
        }
        else
        {
            return pTurn + 1;
        }
    }

    public void fleetDeath(MapManager mapMan)
    {
        List<Player> plyrs = mapMan.mainGame.players;

        foreach (Player p in plyrs)
        {
            for(int fltNum = 0; fltNum < p.Navy.Count; fltNum++)//foreach (Fleet flt in p.Navy)
            {
                Fleet flt = p.Navy[fltNum];
                for (int ship = 0; ship < flt.Flt.Count; ship++)
                {
                    if (flt.Flt[ship].health <= 0)
                    {
                        flt.Flt.RemoveAt(ship);
                        ship--;
                    }
                }

                if (flt.Flt.Count <= 0)
                {
                    int[] Coords = flt.Coordinates;
                    mapMan.Main.mapArray[Coords[0], Coords[1]].plane.GetComponent<Renderer>().sharedMaterial = mapMan.dictVisibleMaterials["Wtr"];
                    mapMan.Main.mapArray[Coords[0], Coords[1]].Occupy = null;
                    mapMan.Main.mapArray[Coords[0], Coords[1]].Owner = null;
                    p.fogExceptionRemove(Coords);
                    p.Navy.RemoveAt(fltNum);
                    p.Navy.TrimExcess();
                    fltNum--;
                }
            }
        }
    }
}
#endregion Game

public class GameManager : MonoBehaviour
{
    #region Members
    public Game gameMan { get; set; }
    public float healAmount { get; set; }

    MapManager mapMan { get; set; }
    public Material Mat { get; set; }
    public static int NumOfPlayers { get; set; }
    #endregion Members

    #region Start / Update

    void Start()
    {
        mapMan = GameObject.Find("mapMain").GetComponent<MapManager>();

        NumOfPlayers = 2;
        //gameMan.round = 1;
    }

    void Update()
    {

    }

    #endregion Start / UpdateS
}
﻿using UnityEngine;
using System.Collections;

#region Battle
public class Battle
{
    Fleet attacker { get; set; }
    Fleet defender { get; set; }
    MapManager mapMan { get; set; }

    public Battle(Fleet Attacker, Fleet Defender, MapManager MapMan)
    {
        attacker = Attacker;
        defender = Defender;
        mapMan = MapMan;

        BattlePhase();
    }

    public void BattlePhase()
    {
        float attackerStr = 0,
            attackerHull = 0,
            attackerDamage = 0,
            dmgMtplyOne = 0,

            defenderStr = 0,
            defenderHull = 0,
            defenderDamage = 0,
            dmgMtplyTwo = 0;

        int count = -1;

        #region Calculate Strength and Hull
        foreach (Ship shp in attacker.Flt)
        {
            attackerStr += shp.strength;
            attackerHull += shp.hull;
        }

        foreach (Ship shp in defender.Flt)
        {
            defenderStr += shp.strength;
            defenderHull += shp.hull;
        }
        #endregion Calculate Strength and Hull

        #region Calculate Damage

        if (attackerStr < defenderHull)
            attackerDamage = Random.Range(0, (defenderHull - attackerStr));
        else
            attackerDamage = attackerStr - defenderHull;

        if (defenderStr < attackerHull)
            defenderDamage = Random.Range(0, (defenderStr / 2));
        else
            defenderDamage = defenderStr - attackerHull;

        #endregion Calculate Damage

        #region Set Attacker/Defender DamageMultiplers

        switch (attacker.Flt.Count)
        {
            case 1:
                dmgMtplyOne = 1f;
                break;
            case 2:
                dmgMtplyOne = 0.5f;
                break;
            case 3:
                dmgMtplyOne = 0.33f;
                break;
            case 4:
                dmgMtplyOne = 0.25f;
                break;
        }

        switch (defender.Flt.Count)
        {
            case 1:
                dmgMtplyTwo = 1f;
                break;
            case 2:
                dmgMtplyTwo = 0.5f;
                break;
            case 3:
                dmgMtplyTwo = 0.33f;
                break;
            case 4:
                dmgMtplyTwo = 0.25f;
                break;
        }

        #endregion Set Attacker/Defender DamageMultiplers

        #region Delegate Damage

        foreach (Ship shp in attacker.Flt)
        {
            count++;
            shp.health -= (defenderDamage * dmgMtplyOne);
        }

        count = -1;

        foreach (Ship shp in defender.Flt)
        {
            count++;
            shp.health -= (attackerDamage * dmgMtplyTwo);
        }

        #endregion Delegate Damage

        mapMan.mainGame.fleetDeath(mapMan);
    }
}
#endregion

public class battleManager : MonoBehaviour
{
    #region Members

    public Battle currentBattle { get; set; }
    Game mainGame { get; set; }
    MapManager mapMan { get; set; }

    #endregion Members

    #region Start / Update

    void Start()
    {
        mainGame = gameObject.GetComponent<GameManager>().gameMan; //Link GameManager Script
        mapMan = GameObject.Find("mapMain").GetComponent<MapManager>(); //Link MapManager Script

    }
    
    void Update()
    {

    }

    #endregion Start / Update

    public void startBattle(Fleet Attacker, Fleet Defender)
    {
        currentBattle = new Battle(Attacker, Defender, mapMan);
    }
}
